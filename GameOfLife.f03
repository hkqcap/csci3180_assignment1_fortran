!
! CSCI3180 Principles of Programming Languages
!
! --- Declaration ---
! I declare that the assignment here submitted is original except for source
! material explicitly acknowledged. I also acknowledge that I am aware of
! University policy and regulations on honesty in academic work, and of the
! disciplinary guidelines and procedures applicable to breaches of such policy
! and regulations, as contained in the website
! http://www.cuhk.edu.hk/policy/academichonesty/
!
! Assignment 1
! Name: Lo Ka Lok
! Student ID: 1155063995
! Email Addr: kllo@phy.cuhk.edu.hk
!
PROGRAM GameOfLifeSimulator
IMPLICIT NONE
CHARACTER(1024) :: filename
CHARACTER(80) :: nameOfPattern
CHARACTER, PARAMETER :: live = "*", dead = "0"
INTEGER :: numOfSteps, numOfRows, numOfColumns
CHARACTER, DIMENSION(100, 80) :: currentPattern, nextPattern
INTEGER :: n = 0
INTEGER :: stillGen = 0

! Main !
CALL getarg(1, filename)
! Parsing the input
OPEN(100, FILE = filename, STATUS = "OLD")
CALL parseInput(100)
CLOSE(100)
OPEN(200, FILE = trim(nameOfPattern) // "for.txt", STATUS = "REPLACE")
17 IF (n < numOfSteps) THEN
	CALL genNextPattern() ! Generate next pattern
	n = n + 1
	IF (isIdentical()) THEN
		stillGen = n - 1! Still life
		CALL printPattern(currentPattern, 200)
		IF (stillGen == 0) WRITE(200, *) "It is a still life initially."
		IF (stillGen == 1) WRITE(200, *) "It is a still life after 1 step."
		IF (stillGen > 1) WRITE(200, "(A,I0,A)") "It is a still life after", stillGen, " steps."
		GOTO 21
	END IF
	currentPattern = nextPattern
	GOTO 17
END IF
stillGen = n
CALL printPattern(currentPattern, 200)
IF (stillGen == 1) WRITE(200, *) "It is still not a still life even after 1 step." ! cunning
IF (stillGen == n .AND. stillGen /= 1) WRITE(200, "(A,I0,A)") "It is still not a still life even after ", stillGen ," steps."
21 CLOSE(200)

CONTAINS

	SUBROUTINE parseInput(unitNo)
		IMPLICIT NONE
		INTEGER, INTENT(IN) :: unitNo
		INTEGER :: i, j ! The loop counter
		CHARACTER(80) :: TMP_LINE
		READ(unitNo, *) nameOfPattern ! Read the name of the pattern (line 1)
		READ(unitNo, *) numOfSteps ! Read the number of steps (line 2)
		READ(unitNo, *) numOfRows, numOfColumns ! Read the number of rows and columns in the pattern (line 3)
		i = 1
		11 IF (i <= numOfRows) THEN
			j = 1
			READ(unitNo, "(A)") TMP_LINE
			12 IF (j <= numOfColumns) THEN
				READ(TMP_LINE(j:j), "(A)") currentPattern(i, j) ! Read the character one by one
				j = j + 1
				GOTO 12
			END IF
			i = i + 1
			GOTO 11
		END IF
	END SUBROUTINE parseInput

	SUBROUTINE printPattern(pattern, unitNo)
		IMPLICIT NONE
		CHARACTER, DIMENSION(100, 80), INTENT(IN) :: pattern
		INTEGER, INTENT(IN) :: unitNo
		INTEGER :: i, j
		i = 1
		13 IF (i <= numOfRows) THEN
			j = 1
			14 IF (j <= numOfColumns) THEN
				WRITE(unitNo, "(A)", advance = "no") pattern(i,j) ! Print with newline suppressed
				j = j + 1
				GOTO 14
			END IF
			WRITE(unitNo,*) "" ! Print a new line
			i = i + 1
			GOTO 13
		END IF
	END SUBROUTINE printPattern

	SUBROUTINE genNextPattern()
		IMPLICIT NONE
		INTEGER :: i, j
		INTEGER :: answer
		i = 1
		15 IF (i <= numOfRows) THEN
			j = 1
			16 IF (j <= numOfColumns) THEN
				answer = countLiveNeighbours(i ,j)
				IF (currentPattern(i, j) == dead) THEN
					IF (answer == 3) nextPattern(i, j) = live ! Birth
					IF (answer /= 3) nextPattern(i, j) = dead ! Overcrowding or Loneliness
				END IF
				IF (currentPattern(i, j) == live) THEN
					IF (answer == 2 .OR. answer == 3) nextPattern(i, j) = live ! Survival
					IF (.NOT.(answer == 2 .OR. answer == 3)) nextPattern(i, j) = dead ! Overcrowsing or Loneliness
				END IF 
				j = j + 1
				GOTO 16
			END IF
			i = i + 1
			GOTO 15
		END IF
	END SUBROUTINE genNextPattern

	INTEGER FUNCTION countLiveNeighbours(i, j)
		IMPLICIT NONE
		INTEGER, INTENT(IN) :: i, j
		INTEGER :: liveCounter
		liveCounter = 0
		! Checking previous row
		IF ((i - 1) >= 1) THEN
			! The previous row is a valid row
			IF ((j - 1) >= 1) THEN
				IF (currentPattern(i - 1, j - 1) == live) liveCounter = liveCounter + 1
			END IF
			IF (currentPattern(i - 1, j) == live) liveCounter = liveCounter + 1
			IF ((j + 1) <= numOfColumns) THEN
				IF (currentPattern(i - 1, j + 1) == live) liveCounter = liveCounter + 1
			END IF
		END IF
		! Checking current row
		IF ((j - 1) >= 1) THEN
			IF (currentPattern(i, j - 1) == live) liveCounter = liveCounter + 1
		END IF
		IF ((j + 1) <= numOfColumns) THEN
			IF (currentPattern(i, j + 1) == live) liveCounter = liveCounter + 1
		END IF
		! Checking the next row
		IF ((i + 1) <= numOfRows) THEN
			IF ((j - 1) >= 1) THEN
				IF (currentPattern(i + 1, j - 1) == live) liveCounter = liveCounter + 1
			END IF
			IF (currentPattern(i + 1, j) == live) liveCounter = liveCounter + 1
			IF ((j + 1) <= numOfColumns) THEN
				IF (currentPattern(i + 1, j + 1) == live) liveCounter = liveCounter + 1
			END IF
		END IF
		countLiveNeighbours = liveCounter
	END FUNCTION
 
	LOGICAL FUNCTION isIdentical()
		LOGICAL :: flag = .TRUE.
		INTEGER :: i, j
		i = 1
		19 IF (i <= numOfRows) THEN
			j = 1
			20 IF (j <= numOfColumns) THEN
				IF (currentPattern(i, j) /= nextPattern(i, j)) flag = .FALSE.
				j = j + 1
				GOTO 20
			END IF
			i = i + 1
			GOTO 19
		END IF
		isIdentical = flag
	END FUNCTION isIdentical
END PROGRAM GameOfLifeSimulator
