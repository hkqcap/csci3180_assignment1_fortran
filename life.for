C
C CSCI3180 Principles of Programming Languages
C
C --- Declaration ---
C I declare that the assignment here submitted is original except for source
C material explicitly acknowledged. I also acknowledge that I am aware of
C University policy and regulations on honesty in academic work, and of the
C disciplinary guidelines and procedures applicable to breaches of such policy
C and regulations, as contained in the website
C http://www.cuhk.edu.hk/policy/academichonesty/
C
C Assignment 1
C Name: Lo Ka Lok
C Student ID: 1155063995
C Email Addr: kllo@phy.cuhk.edu.hk
C
      PROGRAM GameOfLife
      IMPLICIT NONE
C Variable declaration
      CHARACTER*1024 fName
      CHARACTER*1024 pName
      CHARACTER live, dead
      COMMON /param/ live, dead
      INTEGER nSteps, nRows, nCols
      CHARACTER cPtn(100, 80), nPtn(100, 80)
      INTEGER n
      INTEGER idx
      INTEGER stGen
      CHARACTER*5 stGenStr
      CHARACTER*14 outputStr
      LOGICAL isIdt
C Variable initialization
C Obviously, f77 doesn't support variable initialization during declaration, as in those modern language (which is supported in f03!)
      n = 0
      stGen = 0
      live = "*"
      dead = "0"

C Main
      CALL getarg(1, fName)
      CALL parseInput(fName, pName, nSteps, nRows, nCols, cPtn)
      idx = INDEX(pName, " ")
      IF(idx .NE. 0) pName = pName(:idx-1)
      OPEN(200, FILE = pName, STATUS = "UNKNOWN", ERR = 1002)

C Copying cPtn (0th generation) to nPtn
      CALL derpCopy(nPtn,cPtn, nRows, nCols)

 200  IF (n .LE. nSteps) GOTO 202
      GOTO 201
 202  CALL derpCopy(cPtn, nPtn, nRows, nCols)
      CALL genNextPattern(cPtn, nPtn, nRows, nCols)
C Now, cPth contains the nth pattern and nPth contains the (n+1)th pattern
      IF (isIdt(cPtn, nPtn, nRows, nCols)) GOTO 203
      GOTO 204
 203        stGen = n
            WRITE(stGenStr, '(I5.5)') stGen
            CALL trimLeadingZeros(stGenStr)
C Removing trailing whitespace
            idx = INDEX(stGenStr, " ")
            IF(idx .NE. 0) outputStr = stGenStr(:idx-1) // " steps.\r\n"
            IF(idx .EQ. 0) outputStr = stGenStr // " steps.\r\n"
            CALL printPattern(cPtn, 200, nRows, nCols)
            IF (stGen .EQ. 0) WRITE(200, '(A,$)') 
     +      "It is a still life initially.\r\n"
            IF (stGen .EQ. 1) WRITE(200, '(A,$)')
     +      "It is a still life after 1 step.\r\n"
            IF (stGen .GT. 1) WRITE(200, '(A,$)')
     +      "It is a still life after " // outputStr
            GOTO 210
 204  n = n + 1
      GOTO 200

 201  stGen = n - 1
      WRITE(stGenStr, '(I5.5)') stGen
      CALL trimLeadingZeros(stGenStr)
C Removing trailing whitespace
      idx = INDEX(stGenStr, " ")
      IF(idx .NE. 0) outputStr = stGenStr(:idx-1) // " steps.\r\n"
      IF(idx .EQ. 0) outputStr = stGenStr // " steps.\r\n"
      CALL printPattern(cPtn, 200, nRows, nCols)
      IF (stGen .EQ. 1) WRITE(200, '(A,$)') 
     +"It is still not a still life even after 1 step.\r\n"
      IF (stGen .EQ. n - 1 .AND. stGen .NE. 1) WRITE(200, "(A,$)") 
     +"It is still not a still life even after " // outputStr
 210  CLOSE(200)
      
      GOTO 1003
 1002 WRITE(*,*) "ERROR: File cannot be used for writing"
 1003 STOP
      END

C Subroutine for parsing the input text file
      SUBROUTINE parseInput(fName, pName, nSteps, nRows, nCols, cPtn)
      IMPLICIT NONE
C Variables passed by reference
      CHARACTER*1024 fName
      CHARACTER*1024 pName
      INTEGER nSteps, nRows, nCols
      CHARACTER cPtn(100, 80)
C Internal variables
      INTEGER i, j, idx
      CHARACTER*81 tmp
      CHARACTER*81 tmp2

      OPEN(100, FILE = fName, STATUS = "OLD", ERR = 1000)
C Reading the pattern name
      READ(100, "(81A)") tmp
      idx = INDEX(tmp, "\r")
      pName = tmp(:idx-1)
      idx = INDEX(pName, " ")
      pName = pName(:idx-1) // "for.txt"
C Reading the number of generations
      READ(100, *) nSteps
C Reading the number of rows and columns
      READ(100, *) nRows, nCols

C Reading the pattern
      i = 1
 103  IF(i .LE. nRows) GOTO 104
      GOTO 106
 
C Outer for loop content
 104  j = 1
      READ(100, "(81A)") tmp
      idx = INDEX(tmp, "\r")
      tmp2 = tmp(:idx-1)
      tmp = tmp2(1:nCols)
 107  IF(j .LE. nCols) GOTO 105
      i = i + 1
      GOTO 103

C Inner for loop content
 105  cPtn(i, j) = tmp(j:j)
      j = j + 1
      GOTO 107

C The control merges back here
 106  GOTO 1001

C Error handling
 1000 WRITE(*,'(A)') "ERROR: File cannot be opened for reading"
 1001 RETURN
      END

C Subroutine for printing a pattern
      SUBROUTINE printPattern(ptn, uNo, nRows, nCols)
      IMPLICIT NONE
C Variables passed by reference
      INTEGER nRows, nCols
      CHARACTER ptn(100, 80)
      INTEGER uNo
C Internal variables
      INTEGER i, j
            
      i = 1
C WARNING: SPAGHETTI CODE AHEAD
C Outer for loop
 150  IF (i .LE. nRows) GOTO 151
      GOTO 153
 151  j = 1
 152  IF (j .LE. nCols) GOTO 154
      GOTO 155
C Inner for loop
 154  WRITE(uNo, "(A,$)") ptn(i,j) ! Print with newline suppressed
      j = j + 1
      GOTO 152
      
 155  WRITE(uNo,'(A,$)') "\r\n" ! Print a new line
      i = i + 1
      GOTO 150

 153  RETURN
      END

C Subroutine for generating next pattern
      SUBROUTINE genNextPattern(cPtn, nPtn, nRows, nCols)
      IMPLICIT NONE
C Variables passed by reference
      CHARACTER cPtn(100, 80)
      CHARACTER nPtn(100, 80)
      INTEGER nRows, nCols
      CHARACTER live, dead
      COMMON /param/ live, dead
      INTEGER cLN
C Internal variables
      INTEGER i, j
      INTEGER answer

      i = 1
C WARNING: SPAGHETTI CODE AHEAD
C Outer for loop content 
 140  IF (i .LE. nRows) GOTO 142
      GOTO 141
 142  j = 1
 143  IF (j .LE. nCols) GOTO 144
      GOTO 145
C Inner for loop content
 144  answer = cLN(i ,j, cPtn, nRows, nCols)
      IF (cPtn(i, j) .EQ. dead) GOTO 146
      GOTO 147

 146  IF (answer .EQ. 3) nPtn(i, j) = live
      IF (answer .NE. 3) nPtn(i, j) = dead
                        
 147  IF (cPtn(i, j) .EQ. live) GOTO 148
      GOTO 149
 148  IF (answer .EQ. 2 .OR. answer .EQ. 3) nPtn(i, j) = live
      IF (.NOT.(answer .EQ. 2 .OR. answer .EQ. 3)) nPtn(i, j) = dead
      
 149  j = j + 1
      GOTO 143
C End of inner for loop
 145  i = i + 1
      GOTO 140

 141  RETURN
      END

C Function to count live neighbours
      INTEGER FUNCTION cLN(i, j, cPtn, nRows, nCols)
      IMPLICIT NONE
C Variables passed by reference
      INTEGER i, j, nRows, nCols
      CHARACTER cPtn(100, 80)
      CHARACTER live, dead
      COMMON /param/ live, dead
C Internal variables
      INTEGER liveCounter
      liveCounter = 0

C WARNING: SPAGHETTI CODE AHEAD
C Checking previous row
      IF ((i - 1) .GE. 1) GOTO 110
      GOTO 112
C The previous row is indeed a valid row
 110  IF ((j - 1) .GE. 1) GOTO 111
      GOTO 113
 111  IF (cPtn(i - 1, j - 1) .EQ. live) liveCounter = liveCounter + 1
 113  IF (cPtn(i - 1, j) .EQ. live) liveCounter = liveCounter + 1
      IF ((j + 1) .LE. nCols) GOTO 114
      GOTO 112
 114  IF (cPtn(i - 1, j + 1) .EQ. live) liveCounter = liveCounter + 1
      
C     Control merges!

C Checking current row
 112  IF ((j - 1) .GE. 1) GOTO 115
      GOTO 116
 115  IF (cPtn(i, j - 1) .EQ. live) liveCounter = liveCounter + 1
 116  IF ((j + 1) .LE. nCols) GOTO 117
      GOTO 118
 117  IF (cPtn(i, j + 1) .EQ. live) liveCounter = liveCounter + 1

C     Control merges!

C Checking the next row
 118  IF ((i + 1) .LE. nRows) GOTO 130
      GOTO 132
C The next row is indeed a valid row
 130  IF ((j - 1) .GE. 1) GOTO 131
      GOTO 133
 131  IF (cPtn(i + 1, j - 1) .EQ. live) liveCounter = liveCounter + 1
 133  IF (cPtn(i + 1, j) .EQ. live) liveCounter = liveCounter + 1
      IF ((j + 1) .LE. nCols) GOTO 134
      GOTO 132
 134  IF (cPtn(i + 1, j + 1) .EQ. live) liveCounter = liveCounter + 1
      
C     Control merges!
 132  cLN = liveCounter
      RETURN
      END

C Function for checking whether two patterns are identical
      LOGICAL FUNCTION isIdt(cPtn, nPtn, nRows, nCols)
C Variables passed by reference
      CHARACTER cPtn(100, 80), nPtn(100, 80)
      INTEGER nRows, nCols
C Internal variables
      LOGICAL flag
      INTEGER i, j

      flag = .TRUE.

      i = 1
 160  IF (i .LE. nRows) GOTO 161
      GOTO 162
 161  j = 1
 163  IF (j .LE. nCols) GOTO 164
      GOTO 165
 164  IF (cPtn(i, j) .NE. nPtn(i, j)) flag = .FALSE.
      j = j + 1
      GOTO 163
 165  i = i + 1
      GOTO 160

 162  isIdt = flag
      END

C Subroutine for deep copying nPtn TO cPtn
      SUBROUTINE derpCopy(cPtn, nPtn, nRows, nCols)
C Variables passed by reference
      CHARACTER cPtn(100, 80), nPtn(100, 80)
      INTEGER nRows, nCols
C Internal variables
      INTEGER i, j
      i = 1
 170  IF (i .LE. nRows) GOTO 171
      GOTO 172
 171  j = 1
 173  IF (j .LE. nCols) GOTO 174
      GOTO 175
 174  cPtn(i, j) = nPtn(i, j)
      j = j + 1
      GOTO 173
 175  i = i + 1
      GOTO 170

 172  RETURN
      END

      SUBROUTINE trimLeadingZeros(stGenStr)
C Variables passed by reference
      CHARACTER*5 stGenStr
C Internal variables
      INTEGER idx
      
C While loop
 180  idx = INDEX(stGenStr, "0")
      IF(idx .EQ. 1) GOTO 181
      GOTO 182
 181  stGenStr = stGenStr(2:)
      GOTO 180
 182  RETURN
      END
